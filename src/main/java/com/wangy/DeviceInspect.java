package com.wangy;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @author wangy@hadlinks.com
 * @date 2021/10/27 16:54
 */
@Data
public class DeviceInspect {

    @ExcelProperty("资产编号")
    private String iceCuberNumber;
    @ExcelProperty("巡检时间")
    private String inspectionDate;

}
